package org.academiadecodigo.ramsters43;

import java.io.File;

public enum Resource {

    OOPS_PNG("oops.png"),

    INDEX_HTML("index.html"),
    OOPS_HTML("oops.html"),

    FAVICON_ICO("favicon.ico");

    private final String resourceFileName;
    private final String resourceFilePath;

    private final File resourceFile;

    Resource(String resourceFileName){
        this.resourceFileName = resourceFileName;
        resourceFilePath = TcpChatServer.DOCUMENT_ROOT + resourceFileName;
        resourceFile = new File(resourceFilePath);
    }

    public String getResourceFileName(){
        return resourceFileName;
    }

    public String getResourceFilePath() {
        return resourceFilePath;
    }

    public File getResourceFile() {
        return resourceFile;
    }

    public static File getResourceFile(String resourceFileName){
        return new File(TcpChatServer.DOCUMENT_ROOT + resourceFileName);
    }
}
