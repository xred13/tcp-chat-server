package org.academiadecodigo.ramsters43;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum ChatCommand {

    PM("/pm \".+\" .+");

    private final String regexValidationPattern;

    ChatCommand(String regexPattern){
        regexValidationPattern = regexPattern;
    }

    public String getRegexValidationPattern(){
        return regexValidationPattern;
    }

    public static ChatCommand toCommand(String command){

        switch(command){
            case "/pm":
                return PM;
        }

        return null;
    }

    public static String[] getData(ChatCommand chatCommand, String line){

        String[] data = null;

        switch(chatCommand){
            case PM:

                data = new String[3];

                data[0] = "/pm";

                Pattern pattern = Pattern.compile("\"(.+)\"");
                Matcher matcher = pattern.matcher(line);

                if(matcher.find()){

                    data[1] = matcher.group(1);

                    data[2] = line.substring(matcher.end() + 1);

                }

        }

        return data;

    }

    public static boolean isCommand(String line){
        return line != null && line.length() > 0 && line.charAt(0) == '/';
    }

    public static ChatCommand isCommandValid(String line){

        int spaceIndex = line.indexOf(" ");

        if(!isCommand(line) || spaceIndex == -1){
            return null;
        }

        ChatCommand chatCommand = ChatCommand.toCommand(line.substring(0, spaceIndex));

        if(chatCommand == null){
            return null;
        }

        switch(chatCommand){
            case PM:
                if(line.matches(ChatCommand.PM.getRegexValidationPattern())){
                    return ChatCommand.PM;
                }
        }

        return null;

    }

}
