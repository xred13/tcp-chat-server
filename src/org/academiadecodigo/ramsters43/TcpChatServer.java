package org.academiadecodigo.ramsters43;

import org.academiadecodigo.ramsters43.http_helper.*;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class TcpChatServer {

    public static final String DOCUMENT_ROOT = "www/";

    public static void main(String[] args) {

        TcpChatServer tcpChatServer = new TcpChatServer(8080);

        tcpChatServer.start();

    }

    private final ServerSocket serverSocket;

    private final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    private final BlockingQueue<RequestHandler> chatUsers = new LinkedBlockingQueue<>();
    private final BlockingQueue<ClientMessage> clientMessages = new LinkedBlockingQueue<>();

    private TcpChatServer(int port) {

        System.out.println("Creating server...");

        serverSocket = newServerSocket(port);

        System.out.println("Server created!");

    }

    private ServerSocket newServerSocket(int port) {

        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return serverSocket;

    }

    private void start() {

        cachedThreadPool.submit(newMessageQueueHandler());

        while (!serverSocket.isClosed()) {

            cachedThreadPool.submit(
                    new RequestHandler(
                            acceptNewConnection()
                    )
            );

        }
    }

    private Runnable newMessageQueueHandler() {

        return new Runnable() {

            @Override
            public void run() {

                while (true) {

                    if (clientMessages.size() > 0) {

                        System.out.println("Dispatching message!");

                        ClientMessage clientMessage = clientMessages.poll();

                        for (RequestHandler chatUser : chatUsers) {

                            if (chatUser.clientSocket.getInetAddress().equals(clientMessage.inetAddress) && chatUser.clientSocket.getPort() == clientMessage.port) {
                                continue;
                            }

                            if (clientMessage.targetUserName != null && !clientMessage.targetUserName.equals(chatUser.userName)) {
                                continue;
                            }

                            chatUser.pendingMessages.offer(clientMessage);

                        }
                    }
                }
            }
        };

    }

    private Socket acceptNewConnection() {

        System.out.println("Waiting for a new connection...");

        Socket clientSocket = null;

        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("New connection accepted!");
        return clientSocket;

    }

    private class RequestHandler implements Runnable {

        private final Socket clientSocket;

        private String userName;

        private final OutputStream out;

        private final BufferedReader in;

        private HttpHeadersReader headers;

        private final Queue<ClientMessage> pendingMessages = new LinkedList<>();

        private RequestHandler(Socket clientSocket) {

            this.clientSocket = clientSocket;

            out = getOutputStream(clientSocket);

            in = getBufferedReader(clientSocket);

        }

        private OutputStream getOutputStream(Socket socket) {

            OutputStream outputStream = null;

            try {

                outputStream = socket.getOutputStream();

            } catch (IOException e) {

                e.printStackTrace();

            }

            return outputStream;

        }

        private BufferedReader getBufferedReader(Socket socket) {

            BufferedReader bufferedReader = null;

            try {

                bufferedReader = new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()
                        )
                );

            } catch (IOException e) {

                e.printStackTrace();

            }

            return bufferedReader;

        }

        @Override
        public void run() {

            try {

                headers = new HttpHeadersReader(clientSocket.getInputStream());

                String method = headers.getMethod();
                String resourcePath = headers.getPath();

                HttpVerb httpVerb = HttpVerb.stringToHttpVerb(method);

                if (httpVerb == HttpVerb.GET) {

                    handleGetRequest(resourcePath);

                } else if (httpVerb == HttpVerb.CONNECT) {

                    handleConnectRequest();

                } else {

                    handleBadRequests();

                }

            } catch (IOException e) {

                e.printStackTrace();

            } finally {

                close(out);

                close(in);

                chatUsers.remove(this);

                close(clientSocket);

                System.out.println("Client " + userName + " disconnected from the server!");

            }
        }

        private void handleGetRequest(String resourcePath) {

            System.out.println("Handling GET request!");

            if (resourcePath.equals("/")) {
                resourcePath = "/index.html";
            }

            File resourceFile = Resource.getResourceFile(resourcePath);

            if (resourceFile.exists()) {

                HttpHeaders httpHeaders = new HttpHeaders(HttpStatusCode.OK.toResponseHeader())
                        .addField("Content-Length", Long.toString(resourceFile.length()));

                if (HttpMedia.isHtml(resourcePath)) {

                    httpHeaders.addField("Content-Type", "text/html; charset=UTF-8\r\n");

                } else if (HttpMedia.isImage(resourcePath)) {

                    httpHeaders.addField("Content-Type", "image/".concat(HttpMedia.getExtension(resourcePath)));

                }

                sendRequestHeaders(httpHeaders.toString());
                sendResource(resourceFile);

            } else {

                HttpHeaders httpHeaders = new HttpHeaders(HttpStatusCode.NOT_FOUND.toResponseHeader())
                        .addField("Content-Type", "text/html; charset=UTF-8");

                sendRequestHeaders(httpHeaders.toString());
                sendResource(Resource.OOPS_HTML.getResourceFile());

            }

            System.out.println("Finished handling a GET request!");

        }

        private void handleConnectRequest() {

            System.out.println("Handling CONNECT request!");

            userName = headers.getFieldValue("UserName");

            for (RequestHandler chatUser : chatUsers) {
                if (chatUser.userName.equals(userName)) {
                    return;
                }
            }

            System.out.printf("Address: %s:%d associated with username %s.\n", clientSocket.getInetAddress().toString(), clientSocket.getPort(), userName);

            chatUsers.offer(this);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));

            while (!clientSocket.isInputShutdown() && !clientSocket.isOutputShutdown()) {

                try {

                    if (in.ready()) {

                        String line = in.readLine();

                        ClientMessage clientMessage;

                        if (ChatCommand.isCommand(line)) {

                            ChatCommand chatCommand = ChatCommand.isCommandValid(line);

                            if (chatCommand == ChatCommand.PM) {

                                String[] data = ChatCommand.getData(ChatCommand.PM, line);

                                String targetUserName = data[1];
                                String message = data[2];

                                clientMessage = new ClientMessage(userName, message + "\n", targetUserName, clientSocket.getInetAddress(), clientSocket.getPort());

                            } else {
                                continue;
                            }

                        } else {

                            clientMessage = new ClientMessage(userName, line + "\n", clientSocket.getInetAddress(), clientSocket.getPort());

                        }

                        clientMessages.offer(clientMessage);

                    }

                    if (pendingMessages.size() > 0) {

                        ClientMessage clientMessage = pendingMessages.poll();

                        String privateString = "";

                        if (clientMessage.targetUserName != null) {
                            privateString = " (private) ";
                        }

                        bufferedWriter.write(clientMessage.userName + privateString + ":" + clientMessage.message);
                        bufferedWriter.flush();

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void handleBadRequests() {

            System.out.println("Handling a bad request!");

            sendRequestHeaders(HttpStatusCode.BAD_REQUEST.toResponseHeader());
            sendResource(Resource.OOPS_HTML.getResourceFile());

            System.out.println("Finished handling a bad request!");

        }

        private void sendRequestHeaders(String requestHeaders) {

            try {

                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));

                bufferedWriter.write(requestHeaders);
                bufferedWriter.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        private void sendResource(File resourceFile) {

            try {

                FileInputStream fileInputStream = new FileInputStream(resourceFile);

                out.write(fileInputStream.readAllBytes());

                out.flush();

                fileInputStream.close();

            } catch (IOException e) {

                e.printStackTrace();

            }
        }

        private void close(BufferedReader bufferedReader) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void close(Socket socket) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void close(OutputStream outputStream) {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class ClientMessage {

        private final String userName;
        private final String message;
        private final InetAddress inetAddress;
        private final int port;

        private final String targetUserName;

        private ClientMessage(String userName, String message, InetAddress inetAddress, int port) {
            this.userName = userName;
            this.message = message;
            this.inetAddress = inetAddress;
            this.port = port;
            this.targetUserName = null;
        }

        private ClientMessage(String userName, String message, String targetUserName, InetAddress inetAddress, int port) {
            this.userName = userName;
            this.message = message;
            this.targetUserName = targetUserName;
            this.inetAddress = inetAddress;
            this.port = port;
        }

    }
}
