package org.academiadecodigo.ramsters43;

import org.academiadecodigo.ramsters43.http_helper.HttpHeaders;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpChatClient {

    public static void main(String[] args) {

        TcpChatClient tcpChatClient = new TcpChatClient("potato", 8080);

        tcpChatClient.start();

    }

    private final Socket socket;

    private BufferedWriter out;
    private BufferedReader in;

    private final String userName;

    private final BufferedReader terminalReader = new BufferedReader(new InputStreamReader(System.in));

    private final int serverPort;

    private TcpChatClient(String userName, int serverPort) {

        this.userName = userName;
        this.serverPort = serverPort;

        socket = new Socket();

    }

    private BufferedReader getBufferedReader(Socket socket) {

        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bufferedReader;
    }

    private BufferedWriter getBufferedWriter(Socket socket) {

        BufferedWriter bufferedWriter = null;

        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bufferedWriter;
    }

    private void start() {

        connect();

        out = getBufferedWriter(socket);
        in = getBufferedReader(socket);

        run();

        System.out.println("closing");

        close(out);
        close(in);
        close(terminalReader);
        close(socket);

    }


    private void connect() {

        InetSocketAddress serverSocketAddress = null;

        try {
            serverSocketAddress = new InetSocketAddress(InetAddress.getByName("127.0.0.1"), serverPort);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            socket.connect(serverSocketAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void run() {

        sendHeaders();

        while (!socket.isClosed()) {

            if (isTerminalReaderReady()) {

                String line = getTerminalReaderNextLine();

                if (line.equals("/quit")) {
                    break;
                }

                handleTerminalInput(line);

            }

            handleSocketInputStream();

        }
    }

    private void sendHeaders() {

        HttpHeaders headers = new HttpHeaders("CONNECT /chat HTTP/1.0\r\n")
                .addField("UserName", userName);

        try {
            out.write(headers.toString());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getTerminalReaderNextLine() {

        String line = "";

        try {
            line = terminalReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return line;
    }

    private boolean isTerminalReaderReady() {

        boolean isReady = false;

        try {
            isReady = terminalReader.ready();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isReady;
    }

    private void handleTerminalInput(String terminalInput) {

        if (ChatCommand.isCommand(terminalInput)) {

            if(ChatCommand.isCommandValid(terminalInput) == null){
                System.out.println("Unrecognized command!");
                return;
            }

        }

        sendMessage(terminalInput);

    }

    private void sendMessage(String message) {

        try {
            out.write(message + "\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void handleSocketInputStream() {

        try {
            if (in.ready()) {

                String line = in.readLine();

                int separatorIndex = line.indexOf(":");

                String userName = line.substring(0, separatorIndex);
                String message = line.substring(separatorIndex + 1);

                System.out.println(userName + ": " + message);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void close(BufferedReader bufferedReader) {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close(BufferedWriter bufferedWriter) {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close(Socket socket) {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
