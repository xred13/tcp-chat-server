package org.academiadecodigo.ramsters43.http_helper;

public enum HttpVerb {

    GET,
    POST,
    PUT,
    DELETE,
    CONNECT;

    public static HttpVerb stringToHttpVerb(String httpVerb){

        switch (httpVerb){
            case "GET":
                return GET;
            case "POST":
                return POST;
            case "PUT":
                return PUT;
            case "DELETE":
                return DELETE;
            case "CONNECT":
                return CONNECT;
        }

        return null;

    }

}
