package org.academiadecodigo.ramsters43.http_helper;

public enum HttpStatusCode {

    OK(200, "Document Follows"),
    BAD_REQUEST(400, "Bad Request"),
    NOT_FOUND(404, "Not Found");

    private final int statusCode;
    private final String message;

    HttpStatusCode(int statusCode, String message){
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String toResponseHeader(){
        return "HTTP/1.0 " + statusCode + " " + message + "\r\n";
    }
}
