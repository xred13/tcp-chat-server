package org.academiadecodigo.ramsters43.http_helper;

import java.io.*;
import java.util.HashMap;

public class HttpHeadersReader {

    private String method, path, protocol;

    private final HashMap<String, String> fields = new HashMap<>();

    /**
     * Accepts an input stream and reads it expecting an http header
     * Initializes requestLine property as the first line of the header
     * Initializes fields hash map, with each key-value pairs that follow the requestLine
     *
     * @param inputStream http request socket inputStream
     */
    public HttpHeadersReader(InputStream inputStream) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        if((line = bufferedReader.readLine()) != null){

            // gets the request line
            String[] requestLine = line.split(" ");

            method = requestLine[0];
            path = requestLine[1];
            protocol = requestLine[2];

        }

        // gets all the key-value pairs and saves them in the fields hash map
        while ((line = bufferedReader.readLine()) != null) {

            // if we reach the end of our headers
            if (line.equals("")) {
                return;
            }

            int index = line.indexOf(":");

            String key = line.substring(0, index);
            String value = line.substring(index + 2);

            fields.put(key, value);

        }

        // if something happened and we did not reach the end of the request headers
        throw new IOException();
    }

    public String getMethod(){
        return method;
    }

    public String getPath(){
        return path;
    }

    public String getProtocol(){
        return protocol;
    }

    public String getFieldValue(String key){
        return fields.get(key);
    }
}
