package org.academiadecodigo.ramsters43.http_helper;

public class HttpHeaders {

    private StringBuilder headers;

    public HttpHeaders(){
        headers = new StringBuilder();
    }

    public HttpHeaders(String headers){
        this.headers = new StringBuilder(headers);
    }

    public HttpHeaders addField(String key, String value){

        headers.append(key);
        headers.append(": ");
        headers.append(value);

        headers.append("\r\n");

        return this;
    }

    @Override
    public String toString() {
        return headers.toString().concat("\r\n");
    }
}
