package org.academiadecodigo.ramsters43.http_helper;

public class HttpMedia {

    public static boolean isImage(String fileName){

        switch(getExtension(fileName)){
            case "jpg":
            case "png":
            case "ico":
                return true;
            default:
                return false;

        }
    }

    public static boolean isHtml(String fileName){
        return getExtension(fileName).equals("html");
    }

    public static boolean isSupported(String fileName){
        return isHtml(fileName) || isImage(fileName);
    }

    public static String getExtension(String fileName){
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }
}
